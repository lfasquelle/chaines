# S'entraîner



!!! note 

    Les pages "S'entraîner" sont des pages d'exercices avec corrigés qui vous donneront 
    la possibilité de vous entraîner un peu plus sur certains sujets.  
    Vous êtes libre de ne pas les traiter, mais on rappelle que s'exercer (après avoir travaillé
    et appris toutes les pages de cours) est l'unique moyen 
    de progresser.
    
    
    
## Exercice 1


&Eacute;crire une fonction python:

```python
def indiceSecondeOccurrence(lettre, chaine):
    """
    chaine -- chaine de caractères
    lettre -- une lettre

    renvoie l'indice de la seconde occurrence de lettre   trouvée dans chaine. 
    Renvoie -1 si lettre n'est pas présente au moins deux fois.
    
    >>> indiceSecondeOccurrence('e',"cendrees")
    5
    >>> indiceSecondeOccurrence('e',"cendrillon")
    -1
    """
```

??? solution "Un code possible"

    ```python
    def indiceSecondeOccurrence(lettre, chaine):
        """
        chaine -- chaine de caractères
        lettre -- une lettre

        renvoie l'indice de la seconde occurrence de lettre   trouvée dans chaine. 
        Renvoie -1 si lettre n'est pas présente au moins deux fois.
        
        >>> indiceSecondeOccurrence('e',"cendrees")
        5
        >>> indiceSecondeOccurrence('e',"cendrillon")
        -1
        """
        témoin = -1
        nombre_occurrence = 0
        for indice, caractere in enumerate(chaine):
            if caractere == lettre:
                if nombre_occurrence == 0 or nombre_occurrence == 1:   
                    témoin = indice
                    nombre_occurrence += 1
                else:
                    pass
        if nombre_occurrence < 2:
            return -1
        else:
            return témoin
    ```


??? solution "Un autre code possible"

    ```python
    def indiceSecondeOccurrence(lettre, chaine):
        """
        chaine -- chaine de caractères
        lettre -- une lettre

        renvoie l'indice de la seconde occurrence de lettre   trouvée dans chaine. 
        Renvoie -1 si lettre n'est pas présente au moins deux fois.
        
        >>> indiceSecondeOccurrence('e',"cendrees")
        5
        >>> indiceSecondeOccurrence('e',"cendrillon")
        -1
        """
        
        nombre_occurrence = 0
        for indice, caractere in enumerate(chaine):
            if caractere == lettre:
                nombre_occurrence += 1 
                if nombre_occurrence == 2:
                    return indice
                else:   
                    pass   
        return -1
    ```



   
   
   
   
## Exercice 2


&Eacute;crire une fonction python:

```python
def indiceDerniereOccurrence(lettre, chaine):
    """
    chaine -- chaine de caractères
    lettre -- une lettre

    renvoie l'indice de la dernière occurrence de lettre   trouvée dans chaine. 
    Renvoie -1 si lettre n'est pas présente dans chaine.
    
    >>> indiceDerniereOccurrence('e',"cendrees")
    6
    >>> indiceDerniereOccurrence('e',"cendrillon")
    1
    >>> indiceDerniereOccurrence('e',"candrillon")
    -1
    """
```


??? solution "Un code possible"

    ```python
    def indiceDerniereOccurrence(lettre, chaine):
        """
        chaine -- chaine de caractères
        lettre -- une lettre

        renvoie l'indice de la dernière occurrence de lettre   trouvée dans chaine. 
        Renvoie -1 si lettre n'est pas présente dans chaine.
        
        >>> indiceDerniereOccurrence('e',"cendrees")
        6
        >>> indiceDerniereOccurrence('e',"cendrillon")
        1
        >>> indiceDerniereOccurrence('e',"candrillon")
        -1
        """
        
        témoin = -1
        for indice, caractere in enumerate(chaine):
            if caractere == lettre: 
                témoin = indice
                  
        return témoin
    ```
