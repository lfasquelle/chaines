# Le javanais


Le javanais est un argot défini simplement à partir de la langue courante.
Vous pouvez lire [sa définition sur cette page wikipedia](https://fr.wikipedia.org/wiki/Javanais_%28argot%29).




## Exercice 1


&Eacute;crire un code possible pour la fonction python suivante:

```python
def trouve_indice(chaine, lettre):
    """
    chaine -- chaîne de caractères
    lettre -- caractère
    
    renvoie l'indice de la première occurrence de lettre
    dans chaine si lettre est présente dans chaine.
    renvoie -1 si lettre n'est pas présente dans chaine.
    
    >>> trouve_indice("abcdefgh", 'e')
    4
    >>> trouve_indice("abcdefgh", 'z')
    -1
    """
```



??? solution "Un code possible"


    ```python
    def trouve_indice(chaine, lettre):
        """
        chaine -- chaîne de caractères
        lettre -- caractère
        
        renvoie l'indice de la première occurrence de lettre
        dans chaine si lettre est présente dans chaine.
        renvoie -1 si lettre n'est pas présente dans chaine.
        
        >>> trouve_indice("abcdefgh", 'e')
        4
        >>> trouve_indice("abcdefgh", 'z')
        -1
        """
        for k in range(len(chaine)):
            if chaine[k] == lettre:
                return k
        return -1
    ```


## Exercice 2


&Eacute;crire une fonction prenant en entrée un mot et renvoyant ce même mot mais écrit uniquement avec 
des minuscules.


!!! note
    Pour simplifier, on écrira toutes les lettres sans accent. Libre à vous ensuite d'améliorer 
    les fonctions écrites pour qu'elles prennent aussi en compte les accents.
    
    
??? solution "Un code possible"


    ```python
    def minusculise(mot):
        """
        mot -- chaîne de caractères
        
        renvoie le même mot mais 
        dans lequel toute majuscule est devenue minuscule.
        
        >>> minusculise("CoUcou")
        'coucou'
        """
        ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        alphabet = "abcdefghijklmnopqrstuvwxyz"
        
        mot_minus = ''
        for lettre in mot:
            if lettre not in ALPHABET:
                mot_minus = mot_minus + lettre
            else:
                k = trouve_indice(ALPHABET, lettre)
                mot_minus = mot_minus + alphabet[k]
        return mot_minus
    ```







## Exercice 3

 Les règles du javanais :

+ On ajoute 'av' après chaque consonne ou groupe de consonnes (comme par exemple ch, cl, ph, tr,…) d’un mot.
+ Si le mot commence par une voyelle, on ajoute av devant cette voyelle.
+ On ne rajoute jamais av après la consonne finale d’un mot.

Écrire une fonction qui prend en entrée un mot et donne en sortie sa version javanaise.

La syllabe qui vient parasiter le mot (en javanais classique : 'av') sera également un paramètre de la fonction.




!!! example "Exemples"

    + javanais('Abraracourcix','av') :  avabravaravacavourcavix
    + javanais('Abraracourcix','oz') :  ozabrozarozacozourcozix
    + javanais('pythonique','ail') :  pailythailonailiqailue



??? solution "Un code possible"


    Ce [fichier ipynb](fichiers/javanais.ipynb) présente un code possible.
    
    [Sa version statique html](fichiers/javanais.html):
    
    <iframe width="100%" height="500px" 
    src="fichiers/javanais.html">
    </iframe>
