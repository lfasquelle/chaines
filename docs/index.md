# Les chaînes de caractères



 
Une  chaîne de caractères peut être vue comme un simple message entre guillemets.




!!! note

    Tous les programmes se trouvant avant la partie "Pour s'entraîner" doivent être 
    parfaitement maïtriser. Il faut chercher à les coder encore et encore jusqu'à être certain 
    de savoir.

    La partie "Pour s'entraîner" contient de nombreux exercices. Si ces exercices ne sont pas à apprendre
    à fond comme ce qui précède, les traiter avec le plus de sérieux possible dès maintenant vous garantit
    d'apprendre à programmer et vous assure d'avoir pour le reste de l'année des réflexes de pensée déjà suffisamment
    avancés pour apprendre plus facilement ce qui suivra.
