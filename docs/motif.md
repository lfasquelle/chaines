# Motif



Dans les deux premiers  exercices (et les suivants), on utilisera au moins deux chaînes de caractères:

+ une chaîne nommée texte, qui sera un long texte (par exemple le texte d'un roman).
+ une chaîne plus courte, qui sera nommée motif.

L'objectif, dans les deux premiers exercices, est de savoir 
si le motif est présent dans le texte (c'est à dire si motif est un facteur de texte 
où le terme facteur est utilisé au sens défini dans [cette page](facteurs.md)).


## Exercice 1



&Eacute;crire un code possible pour le corps de la fonction suivante:


```python
def positionOK(motif, texte, position):
    """
    motif -- chaîne de caractères
    texte -- chaîne de caractères
    position -- entier, indice de la chaîne texte

    renvoie True si texte[position..position + longueur(motif)-1] = motif
    c'est à dire si le facteur de texte de longueur égale à celle de motif
    et commençant à l'indice position est égal à motif.
    renvoie False si texte[position..position + longueur(motif)-1] != motif.
    
    >>> positionOK('aha', 'abracadabraaha!', 11)
    True
    >>> positionOK('aha', 'abracadabraaha!', 13)
    False
    """
```


??? solution "Un code"

    ```python
    def positionOK(motif, texte, position):
        """
        motif -- chaîne de caractères
        texte -- chaîne de caractères
        position -- entier, indice de la chaîne texte

        renvoie True si texte[position..position + longueur(motif)-1] = motif
        c'est à dire si le facteur de texte de longueur égale à celle de motif
        et commençant à l'indice position est égal à motif.
        renvoie False si texte[position..position + longueur(motif)-1] != motif.
        
        >>> positionOK('aha', 'abracadabraaha!', 11)
        True
        >>> positionOK('aha', 'abracadabraaha!', 13)
        False
        """
        facteur = '' # contiendra une copie de 
                     # texte[position..position + longueur(motif)-1]
            
        # si position + longueur(motif) "sort" du texte:   
        if position + len(motif) > len(texte):
            return False
        
        # dans les autres cas:
        for k in range(position, position + len(motif)):
            facteur = facteur + texte[k]
            
        return facteur == motif
    ```
    
??? solution "Autre solution"

    Dans cette autre solution, on ne construit pas le facteur en entier: on peut s'arrêter
    dès qu'une lettre de texte[position..position + longueur(motif)-1] est distincte de la lettre
    correspondante dans motif.
    
    ```python
    def positionOK(motif, texte, position):
        """
        motif -- chaîne de caractères
        texte -- chaîne de caractères
        position -- entier, indice de la chaîne texte

        renvoie True si texte[position..position + longueur(motif)-1] = motif
        c'est à dire si le facteur de texte de longueur égale à celle de motif
        et commençant à l'indice position est égal à motif.
        renvoie False si texte[position..position + longueur(motif)-1] != motif.
        
        >>> positionOK('aha', 'abracadabraaha!', 11)
        True
        >>> positionOK('aha', 'abracadabraaha!', 13)
        False
        """    
        if position + len(motif) > len(texte):
            return False
         
        for k in range(position, position + len(motif)):
            if texte[k] != motif[k-position]:
                return False
            
        return True
    ```






## Exercice 2


&Eacute;crire un code possible pour le corps de la fonction suivante:


```python
def trouve(motif, texte):
    """
    motif -- chaîne de caractères
    texte -- chaîne de caractères
   
    
    renvoie le plus petit indice de texte tel que 
    texte[indice..indice + longueur(motif)-1] = motif.
    renvoie -1 si motif n'est pas un facteur de texte.
    
    >>> trouve('aha', 'abracadabraaha!')
    11
    >>> trouve('anneau', 'Un anneau pour les diriger tous.')
    3
    >>> trouve('anneau', 'JavaScript est un langage de programmation de scripts principalement employé dans les pages web interactives.')
    -1    
    """
```

??? solution "Un code"

    On utilise la fonction `positionOK` de l'exercice précédent.
    
    ```python
    def trouve(motif, texte):
        """
        motif -- chaîne de caractères
        texte -- chaîne de caractères
       
        
        renvoie le plus petit indice de texte tel que 
        texte[indice..indice + longueur(motif)-1] = motif.
        renvoie -1 si motif n'est pas un facteur de texte.
        
        >>> trouve('aha', 'abracadabraaha!')
        11
        >>> trouve('anneau', 'Un anneau pour les diriger tous.')
        3
        >>> trouve('anneau', 'JavaScript est un langage de programmation de scripts principalement employé dans les pages web interactives.')
        -1    
        """
        for k in range(0, len(texte)-len(motif)+1):
            if positionOK(motif, texte, k):
                return k
        return -1
    ```




## Exercice 3


&Eacute;crire un code possible pour le corps de la fonction suivante:


```python
def remplacePosition(motif, texte, position, mot):
    """
    motif -- chaîne de caractères
    texte -- chaîne de caractères
    position -- entier, indice de la chaîne texte
    mot -- chaîne de caratères     

    renvoie texte modifié:
    - si le facteur de texte de longueur len(motif) et commençant 
      à l'indice position est égal à motif,
      ce facteur est remplacé par mot.
    - si ce facteur n'est pas égal à motif, texte est inchangé.
    
    >>> remplacePosition('a', 'abracadabra', 1, 'i')
    'abracadabra'
    >>> remplacePosition('a', 'abracadabra', 3, 'i')
    'abricadabra'
    >>> remplacePosition('ton père', 'je suis ton père.', 8, 'un gremlin')
    'je suis un gremlin.'
    """
```


!!! note
    Utilisez la fonction  `positionOK` de l'exercice 1.
    
    
??? solution "Un code"


    ```python
    def remplacePosition(motif, texte, position, mot):
        """
        motif -- chaîne de caractères
        texte -- chaîne de caractères
        position -- entier, indice de la chaîne texte
        mot -- chaîne de caratères     

        renvoie texte modifié:
        - si le facteur de texte de longueur len(motif) et commençant 
          à l'indice position est égal à motif,
          ce facteur est remplacé par mot.
        - si ce facteur n'est pas égal à motif, texte est inchangé.
        
        >>> remplacePosition('a', 'abracadabra', 1, 'i')
        'abracadabra'
        >>> remplacePosition('a', 'abracadabra', 3, 'i')
        'abricadabra'
        >>> remplacePosition('ton père', 'je suis ton père.', 8, 'un gremlin')
        'je suis un gremlin.'
        """
        if not positionOK(motif, texte, position):
            return texte
        
        ch = ''
        # on recopie dans ch les lettres avec indice < position
        for k in range(0,position):
            ch = ch + texte[k]
        # on place le mot de remplacement:
        ch = ch + mot
        # on recopie les lettres de texte situées après le motif:
        for k in range(position+len(motif), len(texte)):
            ch = ch + texte[k]
        return ch
    ```


 
