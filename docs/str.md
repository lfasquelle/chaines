# Définir une chaîne de caractères


## Guillemets simples, doubles.

On peut définir une chaîne de caractères en délimitant un message quelconque par
des simples quotes `''` (ou apostrophes) ou par des guillemets (double quotes) `""`.

```
>>> chaine = 'Bonjour le monde.'
>>> chaine
'Bonjour le monde.'
>>> a = "Coucou les loulous."
>>> a
'Coucou les loulous.'
```

On constate que pour python, il n'y a pas de différence entre les deux choix puisqu'il affiche ensuite dans 
l'interpréteur de la même façon.






## Guillemet triple


Le triple guillemet joue  un rôle particulier pour les fonctions (docstring).
On pourra aussi l'utiliser pour conserver la "mise en forme" (passage à la ligne notamment).

Dans un interpréteur:

```
>>> a =  """Un essai pour passer à la ligne.
... Suis-je sur la ligne suivante?
... Et maintenant?
... """
>>> a
'Un essai pour passer à la ligne.\nSuis-je sur la ligne suivante?\nEt maintenant?\n'
>>> print(a)
Un essai pour passer à la ligne.
Suis-je sur la ligne suivante?
Et maintenant?
```

Les passages à la ligne sont conservés mais sous forme de code `\n` (signifiant newline)
dans l'affichage interpréteur par défaut.

La fonction `print` par contre interprète les `\n` et le retour à la ligne se fait effectivement.
 

On aurait pu déclarer de façon équivalente la chaîne précédente en écrivant:


```
>>> a = 'Un essai pour passer à la ligne.\nSuis-je sur la ligne suivante?\nEt maintenant?\n'
>>> a
'Un essai pour passer à la ligne.\nSuis-je sur la ligne suivante?\nEt maintenant?\n'
>>> print(a)
Un essai pour passer à la ligne.
Suis-je sur la ligne suivante?
Et maintenant?
```

## Présence d'une apostrophe dans la chaîne


Lorsqu'une apostrophe est présente dans la chaîne, comme dans le message "J'aime Python.", si on délimite
la chaîne par une apostrophe, python stoppera la chaîne à la seconde apostrophe rencontrée, ce qui provoquera 
une erreur.

```
>>> a = 'J'aime Python.'
  File "<stdin>", line 1
    a = 'J'aime Python.'
              ^
SyntaxError: invalid syntax
```

Nous disposons de plusieurs façons de contourner le problème.

+ La première consiste à délimiter la chaîne par des guillemets (simples ou triples).

```
>>> a = "J'aime Python."
>>> a
"J'aime Python."
```

Il n'y a alors plus d'ambiguïté pour la fermeture de la chaîne.

+ La seconde consiste à échapper l'apostrophe à l'intérieur de la chaîne à l'aide d'un `\` (nommée backslash ou encore
antislah ou contre-oblique ou barre oblique inversée ou encore barre d'échappement)

```
>>> a = 'J\'aime Python.'
>>> a
"J'aime Python."
```

On pourra de même échapper un guillemet lorsqu'on a besoin d'un guillemet dans une chaîne ouverte par des guillemets.

### A vous

&Eacute;crivez dans l'interpréteur une phrase délimitée par des guillemets et contenant des guillemets.


## Type

Le type d'une chaîne de caractères est str (abréviation de string).

```
>>> a = "coucou"
>>> type(a)
<class 'str'>
```




## Concaténation


On peut concaténer (ajouter bout à bout) deux chaînes avec l'opérateur `+`.

```
>>> a = "Bonjour "
>>> b = "les loulous."
>>> c = a + b
>>> c
'Bonjour les loulous.'
```

La fonction `str` permet de convertir certains objets de type non str en string.  




```
>>> a = 7
>>> type(a)
<class 'int'>
>>> str(a)
'7'
>>> type(str(a))
<class 'str'>
```

On voit dans l'exemple ci-dessus que 7 est de type `int` (entier) mais que str(7) est en fait la chaîne `'7'`, `str(7)` est donc
de type str.

### Utilisation

Dans l'exemple ci-dessous, il faut convertir `a`   en str pour pouvoir la concaténer avec le reste de la chaîne (sinon
l'opérateur + devrait agir sur une chaîne et un entier: cela n'aurait pas de sens et provoquerait une erreur):


**Illustration.**

En ne modifiant pas le type, on obtient une erreur:

```
>>> a = 5 + 2
>>> "Les " + a + " nains."
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: can only concatenate str (not "int") to str
```
le message `can only concatenate str (not "int") to str`  nous signale qu'on ne peut concaténer (c'est à dire ajouter bout à bout)
une chaîne et un entier.

!!! attention
    Vous devez apprendre à décrypter ce type de message qui est suffisamment explicite pour corriger
    vous-même vos programmes.

et en modifiant le type:

```
>>> a = 5 + 2
>>> "Les " + str(a) + " nains."
'Les 7 nains.'
```


On pourra utiliser cette technique lorsqu'on veut incorporer le résultat d'un calcul à une chaîne (la page sur les f-strings
vous donnera une autre possibilité pour cela).
