def structure(entier, deroulement):
    return """
    <!DOCTYPE html>
    <html lang="fr">
    <head>
        <meta charset="utf-8">
        <title> somme </title>
        <style>
        html{{font-size: 18px;}}
        body{{margin: 2rem auto; width: 80%;}}
        </style>
    </head>

    <body>

    <p>Pour calculer la somme des entiers de 1 à n, on utilise la fonction python suivante: </p>
    
    <pre>
    <code>
    def somme(n):
    s = 0
    for k in range(1, n+1):
        s = s + k

    return s
    </code>
    </pre>


    <p>Le détail du déroulement avec l'entier {}: </p>
    {}

    </body>
    </html>
    """.format(entier, deroulement)



def somme(n):
    """
    n -- entier > 0
    """
    s = 0
    somme_chaine = '0'
    ch = "<code>s = 0</code> : la variable s contient 0.<br><br>"

    for k in range(1, n+1):
        s = s + k
        somme_chaine = somme_chaine + '+' + str(k)
        ligne = "La variable k a pour valeur {}.<br>".format(k)
        ligne += "<code>s = s+k</code> : la variable s contient la somme {}, soit {}.<br><br>".format(somme_chaine, s)
        ch = ch + ligne + '\n'

    return ch



def creeFichier(nom, chaine):
    """
    nom -- chaîne de caractères, nom d'un fichier à créer
    chaine -- chaîne de caractères, chaîne à inscrire dans le fichier à créer
    """
    # on crée un fichier html qui sera nommé nom:
    with open(nom + '.html', 'w') as f:
        # on écrit la chaine dans ce fichier:
        print(chaine, file=f)


n = 9
creeFichier('somme' + str(n), structure(n, somme(n)))

