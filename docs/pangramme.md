# Pangramme



Un pangramme 
est une phrase comportant toutes les lettres de l'alphabet. 

### Exemples

+ Portez ce vieux whisky au juge blond qui fume.
+ Joyeux, ivre, fatigué, le nez qui pique, Clown Hary skie dans l’ombre.
+  Monsieur Jack, vous dactylographiez bien mieux que Wolf.
+ Hier, au zoo, j'ai vu dix guépards, cinq zébus, un yak et le wapiti fumer.
+ Voyez ce koala fou qui mange des journaux et des photos dans un bungalow.


!!! note

    On estime ici qu'une lettre est présente dès lors qu'elle est présente sans accent
    soit sous sa forme minuscule, soit sous sa forme majuscule.
    
    On ne cherche pas par contre à ce qu'une lettre soit présente sous ses formes accentuées.


## Exercice

&Eacute;crire une fonction python prenant en entrée une chaîne de caractères
et renvoyant True si cette chaîne contient toutes les lettres de l'alphabet et False sinon.




??? solution "Un code possible"


    ```python
    def est_pangramme(phrase):
        alphabet = "abcdefghijklmnopqrstuvwxyz"
        ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        
        for k in range(len(alphabet)):
            if alphabet[k] not in phrase and ALPHABET[k] not in phrase:
                return False
        return True
    ```
    
    Tests:
    
    ```
    >>> est_pangramme("Portez ce vieux whisky au juge blond qui fume.")
    True
    >>> est_pangramme("Buvez ce vieux whisky du juge blond qui fume.")
    False
    ```

 
