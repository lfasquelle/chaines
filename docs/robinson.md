# La suite de Robinson


## Exercice 1



&Eacute;crire un corps possible pour la fonction suivante:

```python
def compte_caractere(chaine, caractere) :
    """ 
    chaine -- chaîne de caractères
    caractere -- un caractère 
    
    renvoie un entier égal au nombre 
    d'occurrences du caractère caractere dans la chaîne chaine.
    >>> compte_caractere("coucou", 'c')
    2
    >>> compte_caractere("abracadabra", 'e')
    0
    """
```


??? solution "Aide"

    Utiliser le principe de compteur déjà utilisé pour 
    [cet exercice qui comptait les e](parcourir.md#exercice_1).
    
    
    




??? solution "Un code possible"

    ```python
    def compte_caractere(chaine, caractere) :
        """ 
        chaine -- chaîne de caractères
        caractere -- un caractère 
        
        renvoie un entier égal au nombre 
        d'occurrences du caractère caractere dans la chaîne chaine.
        >>> compte_caractere("coucou", 'c')
        2
        >>> compte_caractere("abracadabra", 'e')
        0
        """
        
        compteur = 0
        for caract in chaine:
            if caract == caractere:
                compteur += 1
        return compteur
    ```



## Exercice 2 (la suite de Robinson)

Déterminez la logique de passage d'une ligne à la suivante dans ce qui suit:


```
0
10
1110
3110
132110
13123110
23124110
1413223110
1423224110
2413323110
```


??? solution "Réponse"

    Chaque ligne décrit le contenu de la ligne précédente.
    
    + La première ligne est constituée de un 0: on écrit 10 en seconde ligne.
    + La seconde ligne est constituée de un 1 et un 0: on écrit 1110.
    + La troisième ligne est constituée de trois 1 et un 0: on écrit 3110.
    + La quatrième ligne est constituée de un 3, deux 1 et un 0: 132110.
    + On continue: succession de "effectif dans la ligne précédente suivi de valeur".
    
    
    **Attention: les valeurs sont triées en ordre décroissant**. Si le plus grand chiffre présent est 8
    et qu'il est présent 3 fois au total dans la chaîne, la ligne suivante commencera par '38...'. 



Quelle serait la ligne suivant celle-ci:

```
566373785
```


??? solution "Réponse"

    1827262523
    
  
  
!!! note "Référence"
    [wikipedia](https://fr.wikipedia.org/wiki/Suite_de_Robinson)
    
    

## Exercice 3



&Eacute;crire un corps possible pour la fonction suivante:

```python
def ligne_suivante(chaine) :
    """ 
    chaine -- chaine de caractères (constituée de chiffres)
    
    renvoie la chaîne de caractères construite suivant la logique de 
    l'exercice précédent.
    >>> ligne_suivante('0')
    '10'
    >>> ligne_suivante('43325455222')
    '35242342'
    """
```


On utilisera la fonction `compte_caractere` de l'exercice précédent.


??? solution "Un code possible"

    ```python
    def ligne_suivante(chaine) :
        """ 
        chaine -- chaine de caractères (constituée de chiffres)

        renvoie la chaîne de caractères construite suivant la logique de 
        l'exercice précédent.
        >>> ligne_suivante('0')
        '10'
        >>> ligne_suivante('43325455222')
        '35242342'
        """
        ligne_suiv = ''
        
        nombre_9 = compte_caractere(chaine, '9')
        if nombre_9 != 0: ligne_suiv = ligne_suiv + str(nombre_9) + '9'
            
        nombre_8 = compte_caractere(chaine, '8')
        if nombre_8 != 0: ligne_suiv = ligne_suiv + str(nombre_8) + '8'
            
        nombre_7 = compte_caractere(chaine, '7')
        if nombre_7 != 0: ligne_suiv = ligne_suiv + str(nombre_7) + '7'
            
        nombre_6 = compte_caractere(chaine, '6')
        if nombre_6 != 0: ligne_suiv = ligne_suiv + str(nombre_6) + '6'
            
        nombre_5 = compte_caractere(chaine, '5')
        if nombre_5 != 0: ligne_suiv = ligne_suiv + str(nombre_5) + '5'
            
        nombre_4 = compte_caractere(chaine, '4')
        if nombre_4 != 0: ligne_suiv = ligne_suiv + str(nombre_4) + '4'
          
        nombre_3 = compte_caractere(chaine, '3')
        if nombre_3 != 0: ligne_suiv = ligne_suiv + str(nombre_3) + '3'
            
        nombre_2 = compte_caractere(chaine, '2')
        if nombre_2 != 0: ligne_suiv = ligne_suiv + str(nombre_2) + '2'
            
        nombre_1 = compte_caractere(chaine, '1')
        if nombre_1 != 0: ligne_suiv = ligne_suiv + str(nombre_1) + '1'
            
        nombre_0 = compte_caractere(chaine, '0')
        if nombre_0 != 0: ligne_suiv = ligne_suiv + str(nombre_0) + '0'
            
        return ligne_suiv
    ```
    
    
+ La solution proposée ci-dessus pour la fonction `ligne_suivante` fait apparaître beaucoup de lignes très 
proches les unes des autres. En précence de telles répétitions, vous devez toujours vous demander
s'il n'est pas possible de **factoriser** le code, c'est à dire de le réduire, notamment en utilisant une boucle `for`.  
Si ce n'est pas déjà fait, faîtes cette réduction.
    

??? solution "Code  possible"

     
     
    
    
    ```python
    def ligne_suivante(chaine) :
        """ 
        chaine -- chaine de caractères (constituée de chiffres)

        renvoie la chaîne de caractères construite suivant la logique de 
        l'exercice précédent.
        >>> ligne_suivante('0')
        '10'
        >>> ligne_suivante('43325455222')
        '35242342'
        """
        ligne_suiv = ''
        
        for chiffre in '9876543210':
            nombre = compte_caractere(chaine, chiffre)
            if nombre != 0: ligne_suiv = ligne_suiv + str(nombre) + chiffre
            
        return ligne_suiv
    ```
    
    !!! important
    
        On rappelle que la fonction `str` utilisée dans le code précédent permet 
        de convertir un type donné (ici un int, résultat de la fonction `compte_caractere` définie
        dans l'exercice de début de cette page) en une chaîne de caractères.
        
        Cette conversion est nécessaire: on ne peut pas concaténer un int et un str.
    


 
        
        
     
    

    
    
## Exercice 4



&Eacute;crire un corps possible pour la fonction suivante:



```python
def triangleRobinson(chaine, nombre_lignes) :
    """ 
    chaine -- chaine de caractères
    nombre_lignes -- entier > 0
    
    renvoie une chaîne de caractères, 
    constituée de nombre_lignes lignes,
    chaque ligne étant construite à partir de la précédente
    par le principe de Robinson précédent.    
    """
```


Par exemple `print(triangleRobinson('0', 8))`  donne:


```
0
10
1110
3110
132110
13123110
23124110
1413223110
```


On utilisera bien sûr la fonction de l'exercice précédent.

??? solution "Un code possible"




    ```python
    def triangleRobinson(chaine, nombre_lignes) :
        """ 
        chaine -- chaine de caractères
        nombre_lignes -- entier > 0
        
        renvoie une chaîne de caractères, 
        constituée de nombre_lignes lignes,
        chaque ligne étant construite à partir de la précédente
        par le principe de Robinson précédent.    
        """
        triangle = ''
        ligne = chaine
        for i in range(nombre_lignes) :
            triangle += ligne + '\n'
            ligne = ligne_suivante(ligne)
        return triangle
    ```
    
    
    
    
    
    
    
## Exercice 5




&Eacute;crire un corps possible pour la fonction suivante:



```python
def arret(chaine):
    """
    chaine -- chaine de caractères
    
    renvoie une chaîne de caractères constituée des lignes
    de la suite de Robinson partant de chaîne.
    Cette chaîne s'arrête dès que deux lignes sont identiques
    ou dès que le nombre de lignes est égal à 20.
    """
```


??? solution "Un code possible"

    [fichier ipynb](fichiers/robinson.ipynb)
    
    [Version statique html](fichiers/robinson.html)
