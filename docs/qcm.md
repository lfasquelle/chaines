#  QCM




## QCM 1

  
Qu'obtient-on avec:

```
>>> 'C'est l'embarras.'
```

- [ ] L'affichage de la chaîne de caractères `C'est l'embarras.`.
- [ ] Une erreur.
- [ ] L'affichage de la chaîne de caractères `C`.
- [ ] L'affichage des trois chaînes de caractères `C`, `est l`, `embarras`.


??? solution "Réponse"

    - [ ] L'affichage de la chaîne de caractères `C'est l'embarras.`.
    - [X] Une erreur.
    - [ ] L'affichage de la chaîne de caractères `C`.
    - [ ] L'affichage des trois chaînes de caractères `C`, `est l`, `embarras`.
    
    Le problème est l'utilisation dans la chaîne d'un symbole qui ouvre la chaîne.
    On peut corriger par exemple par `"C'est l'embarras."`.
    
    
    
    
## QCM 2


Pour écrire correctement, en python, la chaîne "C'est l'embarras.", quelles sont les propositions correctes:

- [ ] "C'est l'embarras."
- [ ] "C\'est l\'embarras."
- [ ] 'C\'est l\'embarras.'
- [ ] 'C/'est l/'embarras.'



??? solution "Réponse"

    - [X] "C'est l'embarras."
    - [X] "C\'est l\'embarras."
    - [X] 'C\'est l\'embarras.'
    - [ ] 'C/'est l/'embarras.'
    
    
## QCM 3

En python, le code suivant:

```
>>> "3" + 2
```

donne:

- [ ] 32
- [ ] '32'
- [ ] une erreur
- [ ] 5
- [ ] '5'


??? solution "Réponse"

    - [ ] 32
    - [ ] '32'
    - [X] une erreur
    - [ ] 5
    - [ ] '5'
    
    L'erreur est: `TypeError: can only concatenate str (not "int") to str.`
    
    Remarque.
    D'autres langages pourraient interpréter différemment. C'est le cas par exemple de JavaScript.
    En JavaScript, "3" + 2 donnerait "32". 
    Vous pouvez visualiser le code de [cette page html](fichiers/troisdeux.html) à l'aide d'un clic droit/code source
    de la page pour constater cela.   
    Pourquoi?  
    Le choix fait en JavaScript lors d'une somme d'un string et d'un number est de convertir
    le nombre en string.
    
    En python, lorsqu'on ajoute un int et un float, le comportement, même s'il semble plus "normal", est tout 
    à fait analogue. On ne peut pas ajouter a priori un int et un float puisque ces deux types de nombres
    sont codés en machine de façons complètement différentes (et l'addition est donc dans les deux cas un algorithme
    complètement différent). Python convertit donc systématiquement la variable de type int en une variable 
    de type float pour pouvoir faire l'addition (et additionne donc en fait deux flottants).



## QCM 4

Quel est le résultat du code python:

```
>>> len("Salut les loulous!")
```

- [ ] 20
- [ ] 17
- [ ] 16
- [ ] 18


??? solution "Réponse"

    - [ ] 20
    - [ ] 17
    - [ ] 15
    - [X] 18
    
    La longueur d'une chaîne est le nombre de caractères. Les blancs, les symboles
    de ponctuation sont des caractères comme les autres.  
    Les guillemets ne doivent pas être comptés, ils servent à délimiter la chaîne.
    


## QCM 5


Qu'obtient-on avec le code python:

```
>>> ch = "python"
>>> ch[2]
```

- [ ] 'y'
- [ ] 't'
- [ ] 'py'
- [ ] 'pyt'

??? solution "Réponse"

    - [ ] 'y'
    - [X] 't'
    - [ ] 'py'
    - [ ] 'pyt'


    Le caractère d'indice 2 est 't' car la numérotation commence à 0.
    
    
## QCM 6



Qu'obtient-on avec le code python:

```
>>> ch = "python"
>>> ch[-2]
```

- [ ] 'n'
- [ ] 'o'
- [ ] 'h'
- [ ] 'on'

??? solution "Réponse"

    - [ ] 'n'
    - [X] 'o'
    - [ ] 'h'
    - [ ] 'on'

    La numérotation ch[-2] est un raccourci pour ch[len(ch)-2].
    Comme le dernier caractère a pour indice len(ch)-1, l'avant-dernier a pour indice len(ch)-2.
    
    
## QCM 7


Après le code python:

```
>>> ch = "python"
>>> ch[1] = 'i'
```
    
    
la variable ch a pour valeur:


- [ ] 'pithon'
- [ ] 'iython'
- [ ] 'python'
- [ ] n'a plus de valeur.


??? solution "Réponse"


    - [ ] 'pithon'
    - [ ] 'iython'
    - [X] 'python'
    - [ ] n'a plus de valeur.
    
    
    L'instruction  `ch[1] = 'i'` n'a aucun effet sur la valeur de ch. Cette instruction déclenche une erreur
    car le type str est un type immuable.
    
    
## QCM 8


On considère la fonction python suivante:

```python
def devore(mot):
    ch = ''
    for indice, lettre in enumerate(mot):
        if indice % 2 == 0:
            ch = ch + lettre
    return ch
```

L'instruction:

```
>>> devore("python")
```

donne:

- [ ] une erreur
- [ ] la chaîne vide
- [ ]  'pto'
- [ ] 'yhn'
 


??? solution "Réponse"



    - [ ] une erreur
    - [ ] la chaîne vide
    - [X]  'pto'
    - [ ] 'yhn'
  


    `if indice % 2 == 0:`  peut se lire "Si indice est pair".
    Dans ch, on accumule (par concaténation) les lettres d'indice pair.
    Comme la numérotation commence à 0, la lettre p d'indice 0 est une lettre d'indice pair, ce sera donc
    la première lettre "accumulée" dans ch. 
