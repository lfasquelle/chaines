# Entraînement




Tous les exercices qui suivent n'utilisent que les notions déjà vues ou rappelées.

Rappelons que les exercices d'entraînement ne sont pas à connaître parfaitement comme ceux des pages précédentes
mais prendre le temps de chercher de façon sérieuse à coder plusieurs des exercices qui suivent est **indispensable**
pour apprendre à programmer.


Rappelons ici qu'il est préférable de traiter à fond trois ou quatre exercices plutôt
que d'en traiter beaucoup plus mais superficiellement.   
Dans la recherche d'un exercice, consulter une solution n'a de sens qu'après un temps de recherche assez long.

Lors de votre temps de recherche, ne négligez jamais le travail sur brouillon:

- Réalisez des schémas au brouillon pour visualiser le problème posé.
- Tentez de rédiger un algorithme en français, avec vos propres mots, avant de vous lancer dans la programmation sur machine. 
Il faut penser la démarche algorithmique "naturelle" avant de se préoccuper d'éventuels soucis de syntaxe.

Pour vérifier un premier script:

- Pensez aux affichages intermédiaires (des `print()`), pour visualiser l'évolution des variables et éventuellement ajuster votre code.
- Pensez à tester en ne négligeant pas les cas "extrêmes".
     




